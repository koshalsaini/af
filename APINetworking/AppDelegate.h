//
//  AppDelegate.h
//  APINetworking
//
//  Created by Click Labs 105 on 11/19/15.
//  Copyright (c) 2015 Koshal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

