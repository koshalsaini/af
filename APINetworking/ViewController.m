//
//  ViewController.m
//  APINetworking
//
//  Created by Click Labs 105 on 11/19/15.
//  Copyright (c) 2015 Koshal. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *image;

@end

@implementation ViewController
@synthesize image;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self getApi];
}
-(void) getApi{
        
    NSURL *url = [NSURL URLWithString:@"http://www.hdiphone6wallpaper.com/wp-content/uploads/Nature/FTP2/Trestle%20iPhone%206%20Wallpaper.jpg"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFImageResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        image.image = responseObject;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
    }];
    
    [operation start];
    

    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
